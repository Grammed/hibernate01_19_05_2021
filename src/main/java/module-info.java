open module Day02_BankApp{
        requires java.persistence;
        requires org.hibernate.orm.core;
        requires java.sql;
        requires net.bytebuddy;
        requires java.xml.bind;
        requires com.fasterxml.classmate;
}
