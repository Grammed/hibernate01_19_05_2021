package org.example;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

// Example of Singleton
public final class EntityManagerSingleton {
	private static EntityManagerFactory emf;
	private static EntityManager em;

	private EntityManagerSingleton() {}

	public static EntityManager getInstance() {
		if (em == null) // will only make one
		emf = Persistence.createEntityManagerFactory("myPersistenceUnit");
		em = emf.createEntityManager();

		return em;
	}

	//  makes it easier to know what instance you are getting.
	public static EntityManager getEmanag() {
		return getInstance();
	}


	public static void close() {
		em.close();
		em = null;
		emf.close();
		emf = null;
	}
}