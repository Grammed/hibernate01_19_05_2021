package org.example.models;


import javax.persistence.*;
import java.util.Objects;

@Entity
public class BankAccount {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "IBAN", length = 45, nullable = false, unique = true)
	private String iban;
	@Column(name = "AccountHolderFullName", length = 150, nullable = false)
	private String name;
	@Column(name = "Balance", precision = 2, nullable = false)
	private double balance;

public Long getId() {
	return id;
}

public String getIban() {
	return iban;
}

public String getName() {
	return name;
}

public double getBalance() {
	return balance;
}

public void setId(Long id) {
	this.id = id;
}

public void setIban(String iban) {
	this.iban = iban;
}

public void setName(String name) {
	this.name = name;
}

public void setBalance(double balance) {
	this.balance = balance;
}

@Override
public boolean equals(Object o) {
	if (this == o) return true;
	if (o == null || getClass() != o.getClass()) return false;
	BankAccount that = (BankAccount) o;
	return Double.compare(that.balance, balance) == 0 && id.equals(that.id) && iban.equals(that.iban) && name.equals(that.name);
}

@Override
public int hashCode() {
	return Objects.hash(id, iban, name, balance);
}

@Override
public String toString() {
	return "BankAccount{" +
	"id=" + id +
	", iban='" + iban + '\'' +
	", name='" + name + '\'' +
	", balance=" + balance +
	'}';
}
}
