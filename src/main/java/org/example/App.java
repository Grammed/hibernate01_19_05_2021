/*
Use singleton with:
EntityManagerSingleton.getInstance()
Create a table from an annotated model (pojo)

Persistence.generateSchema("myPersistenceUnit", null);
Hibernate: alter table BankAccount add constraint UK_bkbcgrsidxgrodtiohbs917qn unique (IBAN)
*/
package org.example;


import org.example.models.BankAccount;
import org.example.repository.BankAccountRepositoryException;
import org.example.repository.BankAccountRepositoryImp01;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class App {

   public static void main(String[] args) throws BankAccountRepositoryException {
      Logger logger = Logger.getLogger("org.hibernate");
      logger.setLevel(Level.WARNING);
      App app = new App();
      app.startApp();



   }

   public void startApp() throws BankAccountRepositoryException {
      BankAccountRepositoryImp01 crudBankAccounts = new BankAccountRepositoryImp01();

      /*BankAccount bankAccount = new BankAccount();
      bankAccount.setIban("BE13 5264 1233 3293");
      bankAccount.setName("Not at all");
      bankAccount.setBalance(2_570_000);
      crudBankAccounts.addNewBankAccount(bankAccount);*/

      //BankAccount retrievedAccount = crudBankAccounts.retrieveAccountById(6L);
      //System.out.println(retrievedAccount);
      //System.out.println(crudBankAccounts.retrieveAccountById(8L));
      //crudBankAccounts.removeAccountById(8L);

      //crudBankAccounts.removeAccountByMerge(retrievedAccount);
      //crudBankAccounts.deposit(6L, 2_000_000L);
      ArrayList<BankAccount> listAll = (ArrayList<BankAccount>) crudBankAccounts.getAllAccounts();
      ArrayList<BankAccount> listBetween = (ArrayList<BankAccount>) crudBankAccounts.getAccountBalancesBetween(1500000.00, 4570000.00 );

      listAll.forEach(System.out::println);
      System.out.println("List two");
      listBetween.forEach(System.out::println);


   }

}
