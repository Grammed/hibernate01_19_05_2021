package org.example.repository;

public class BankAccountRepositoryException extends Exception{

	public BankAccountRepositoryException() {
		super();
	}

	public BankAccountRepositoryException(String message) {
		super(message);
	}
}
