package org.example.repository;

import org.example.models.BankAccount;

import java.util.List;

public interface BankAccountRepository {
	BankAccount addNewBankAccount(BankAccount bankAccount);
	BankAccount retrieveAccountById(Long id) throws BankAccountRepositoryException;
	void removeAccountById(Long id) throws BankAccountRepositoryException;
	void removeAccountByMerge(BankAccount bankAccount);
	boolean deposit(Long id, Long amount) throws BankAccountRepositoryException;
	List<BankAccount> getAllAccounts();
	List<BankAccount> getAccountBalancesBetween(double min, double max);
}
