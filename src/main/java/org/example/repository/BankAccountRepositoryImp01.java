package org.example.repository;

import static org.example.EntityManagerSingleton.*;
import org.example.models.BankAccount;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;


public class BankAccountRepositoryImp01 implements BankAccountRepository {
EntityManager em = getEmanag();


@Override
public BankAccount addNewBankAccount(BankAccount bankAccount) {
	em.getTransaction().begin();
	em.persist(bankAccount);
	em.getTransaction().commit();

	return bankAccount;
}

@Override
public BankAccount retrieveAccountById(Long id) throws BankAccountRepositoryException {
	BankAccount retrievedAccount = em.find(BankAccount.class, id);
	if (retrievedAccount == null) {
		throw new BankAccountRepositoryException("Bank account " + id + " not found");
	} else {
		System.out.println("Account " + id + " retrieved: ");
	}
	return retrievedAccount;
}

@Override
public void removeAccountById(Long id) throws BankAccountRepositoryException {
	BankAccount retrievedAccount = retrieveAccountById(id);
	em.getTransaction().begin();
	em.remove(retrievedAccount);
	em.getTransaction().commit();
}

@Override
public void removeAccountByMerge(BankAccount bankAccount) {
	// This line creates pojo that is def not in persistent state:
	BankAccount usingMergeToDeleteWithDetatchedPojoRatherThanForUpdating = em.merge(bankAccount);

	em.getTransaction().begin();
	em.remove(usingMergeToDeleteWithDetatchedPojoRatherThanForUpdating);
	em.getTransaction().commit();
}

@Override
public boolean deposit(Long id, Long amount) throws BankAccountRepositoryException {
	// This is all about updating a Managed Object while it is Session Persistent ( er.. persistent in an active Hibernate session)
	BankAccount managedAccount = retrieveAccountById(id);
	if(amount > 0) {
		em.getTransaction().begin();
		managedAccount.setBalance(managedAccount.getBalance() + amount);
		em.getTransaction().commit();
		return true;
	}
	return false;
}

@Override
public List<BankAccount> getAllAccounts() {
	TypedQuery<BankAccount> query = em.createQuery("SELECT ba FROM BankAccount ba",
	BankAccount.class);
	List<BankAccount> bankAccounts = query.getResultList();
	return bankAccounts;
}

@Override
public List<BankAccount> getAccountBalancesBetween(double min, double max) {
	TypedQuery<BankAccount> query = em.createQuery("SELECT ba FROM BankAccount ba " +
	"WHERE (ba.balance >= :minAmount AND  ba.balance <= :maxAmount)",
	BankAccount.class);

	query.setParameter("minAmount", min );
	query.setParameter("maxAmount", max );

	List<BankAccount> bankAccounts = query.getResultList();
	return bankAccounts;
}

}
